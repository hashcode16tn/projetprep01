package org.hashcode.src;

import java.util.ArrayList;

public class Map {
	String[][] map;
	ArrayList<Pos> cleaned;
	ArrayList<Pos> A;
	ArrayList<Pos> B;
	ArrayList<Pos> C;
	ArrayList<Pos> D;
	int vides = 0;
	int startsNbr = 0;
	ArrayList<Pos> starts;

	public Map(Fichier fichier) {

		A = new ArrayList<Pos>();
		B = new ArrayList<Pos>();
		C = new ArrayList<Pos>();
		D = new ArrayList<Pos>();
		cleaned = new ArrayList<Pos>();

		for (int i = 0; i < fichier.nbrOfLines(); i++)
			for (int j = 0; j < fichier.getLine(i).length(); j++) {
				map[i][j] = new Character(fichier.getLine(i).charAt(j))
						.toString();
				switch (map[i][j]) {
				case " ":
					vides++;
				case "A":
					A.add(new Pos(i, j));
				case "B":
					B.add(new Pos(i, j));
				case "C":
					C.add(new Pos(i, j));
				case "D":
					D.add(new Pos(i, j));
				case "S":
					starts.add(new Pos(i, j));
				default:
					vides++;

				}
			}
	}

	public int getNbCase(int width, int length) {
		int Nb = width * length;
		return Nb;
	}

	public ArrayList<Pos> getListStartPos() {
		return starts;
	}

	public boolean isBlocked(int x, int y) {
		boolean B = false;
		if (map[x][y].equals("X"))
			B = true;
		return B;

	}

	public boolean wasCleaned(Pos pos) {
		return cleaned.contains(pos);
	}

	public void clean(Pos pos) {
		cleaned.add(pos);
	}

	public ArrayList<TypeDechet> getListTyp() {
		ArrayList<TypeDechet> types = new ArrayList<TypeDechet>();
		if (!A.isEmpty())
			types.add(TypeDechet.A);
		if (!B.isEmpty())
			types.add(TypeDechet.B);
		if (!C.isEmpty())
			types.add(TypeDechet.C);
		if (!D.isEmpty())
			types.add(TypeDechet.D);

		return types;

	}

	public boolean isAllClean() {
		return cleaned.size() == (A.size() + B.size() + C.size() + D.size());
	}
}
