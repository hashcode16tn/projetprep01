package org.hashcode.src;

public class Pos {
	
		int x,y;
		
		public Pos(int x, int y ){
			this.x=x;
			this.y=y;
		}
		
		public int getX()
		{
			return x;
		}
		public int getY()
		{
			return y;
		}

		public void setX (int x )
		{
			this.x=x;
		}
		public void setY (int x )
		{
			this.y=y;
		}
}
