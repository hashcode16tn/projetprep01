package org.hashcode.src;

import java.util.ArrayList;

import org.hashcode.ressource.Node;
import org.hashcode.ressource.Tree;

import com.sun.javafx.geom.Vec2f;

public class Simulation {

	ArrayList<String> types;

	int longueur;
	int largeur;
	int dechets = 0;
	int[] pointer1 = new int[2];
	int[] pointer2 = new int[2];
	Node n;


	Map map ;
	ArrayList<Robot> robots ;
	ArrayList<Vec2f> starts ;

	ArrayList<Node> Trees;

	final static Integer MAX_DEPTH = 10 ;

	public Simulation(Map map, ArrayList<Robot> robots) {
		this.map = map ;
		this.robots = robots ;
		starts = map.getStartPoints() ;
		run() ;
	}

	public void run() {
		for(Robot robot : robots){
			for(Vec2f p : starts){
				simulateFrom(p, robot) ;
			}
		}
	}


	private void simulateFrom(Vec2f p, Robot robot) {
		// TODO Auto-generated method stub
		Tree t = createTree(p, robot) ;
		String shortestPath = t.getShortestPath() ;
		for(int i = 0 ; i < shortestPath.length() ; i++){
			// Compute results
		}
	}

	private Tree createTree(Vec2f p , Robot robot) {
		// TODO Auto-generated method stub
		Tree t = new Tree((int) p.x,(int) p.y);
		Node root = t.root() ;

		addFourChildren((int)p.x, (int)p.y, root) ;

		createTreeRecu(root) ;

		return t ;
	}

	private void createTreeRecu(Node n) {
		// TODO Auto-generated method stub
		for(Node child : n.getChildren()){
			addFourChildren(child.getX(), child.getY(), child);
			if(!map.isAllClean() || child.getDepth() < MAX_DEPTH) // stop condition
				createTreeRecu(child) ;
		}
	}

	private boolean correctIndex(int x, int y) {
		if (x >= 0 && y >= 0 && x < largeur && y < longueur){
			if(!map.isBlocked(x, y)){
				if(map.isDechet(x, y) && robot.canClean(map.getTyp(x, y))){
					map.clean(x, y) ;
				}
				return true;
			}
		}
		return false;
	}

	private void addFourChildren(int x, int y, Node p) {
		if (correctIndex(x + 1, y)){
			p.addChild(x, y);
		}
		if (correctIndex(x, y + 1)){
			p.addChild(x, y);
		}
		if (correctIndex(x - 1, y)){
			p.addChild(x, y);
		}
		if (correctIndex(x, y - 1)){
			n.addChild(x, y);
		}
	}

}
