package org.hashcode.src;

public class Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Read file
		FileManager fm = new FileManager() ;
		Fichier f = fm.getFile("map.txt") ;
		Map map = new Map(f);
		ArrayList<Robot> lsRobots = Robot.getRobots(fm.getFile("robots.txt")) ;
		Simulation sm = new Simulation(map, lsRobots) ;
		Fichier output = sm.getResults() ;

		fm.printFile(output);

	}

}
